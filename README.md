![QtiPlot logo](qtiplot_logo.png) [Visit
QtiPlot webpage](http://www.qtiplot.com/)

# QtiPlot - Installation Notes

## License:

QtiPlot is distributed under the [GNU General Public License](http://www.gnu.org/licenses/gpl.html). Thus it is ["free software"](http://www.gnu.org/philosophy/free-sw.html). "Free software" is a matter of liberty, not price. To understand the concept, you should think of "free" as in "free speech", not as in "free beer". "Free software" is also often called [Open Source, FOSS, or FLOSS](http://en.wikipedia.org/wiki/Alternative_terms_for_free_software).

## General requirements:

*	You need to install the following libraries: [Qt (>= 4.5.0)](https://www.qt.io/download/), [GSL](http://www.gnu.org/software/gsl/), [muParser (1.32)](http://beltoforion.de/article.php?a=muparser), [zlib](http://www.zlib.net/) and [libpng](http://www.libpng.org/pub/png/libpng.html).

*	You also need to build and install the slightly modified versions of the [Qwt (5.2)](http://qwt.sourceforge.net/index.html) and [QwtPlot3D](http://qwtplot3d.sourceforge.net/) libraries - included.

*	For the export of 2D plots to TeX, you need to download and install [QTeXEngine](http://soft.proindependent.com/qtexengine/) - included.

*	If you need to convert random grid XYZ datasets to matrices you must download and install [ALGLIB (2.6)](http://www.alglib.net/).

*	If you want to perform ANOVA statistic calculations you must download [TAMUANOVA](http://www.stat.tamu.edu/~aredd/tamuanova/) - included.

*	If you need Python scripting support, don't forget to download and install [Python](https://www.python.org/), [SIP](https://riverbankcomputing.com/software/sip/download) and [PyQt4](https://riverbankcomputing.com/software/pyqt/download).

*	In order to build QtiPlot you also need `qmake`, which is distributed as a part of Qt.

### Compiling QtiPlot from source:

1.	Install all the dependencies listed above either from your package manager or from the sources. If you are using precompiled packages, make sure you also install the corresponding development packages (`*-dev` or `*-devel`). If you want to display the manual from within QtiPlot (you can also view it with your favorite browser), you also need the Qt Assistant, which sometimes comes in a separate package (`qt4-dev-tools` for Ubuntu edgy).

2.	Open a terminal window.

3.	Download and unpack the this repository.

4.	Compile tamu_anova:

		cd qtiplot/3rdparty/tamu_anova
	    ./configure; make

5.	Go to the main directory and adjust build.conf

6.	Build qtiplot:

		qmake && make

7.	Install QtiPlot on your system (as root):

		make install